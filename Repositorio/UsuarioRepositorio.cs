﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio
{
    public static class UsuarioRepositorio
    {
        public static Usuario ObterUsuarioPorLogin(string login)
        {
            using (var conection = Conexao.ConexaoDB.Conectar())
            {
                var sql = Scripts.Usuario.ObterUsuarioPorLogin;

                SqlCommand cmd = new SqlCommand(sql, conection);
                cmd.Parameters.Add("Login", SqlDbType.VarChar).Value = "%" + login + "%";
                Usuario usuario = null;
                var rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    usuario = new Usuario {
                        IdUsuario = Convert.ToInt32(rd["id"]),
                        IdPessoaFisica = Convert.ToInt32(rd["IdPessoaFisica"]),
                        Nome = Convert.ToString(rd["Nome"]),
                        Login = Convert.ToString(rd["Login"]),
                        Senha = Convert.ToString(rd["Senha"]),
                        DataInclusao = (rd["DataInclusao"] != null) ? Convert.ToDateTime(rd["DataInclusao"]) : DateTime.MinValue,
                        Ativo = Convert.ToBoolean(rd["Ativo"])
                    };
                }


                return usuario;
            }
        }

        public static bool ValidaLogin(string login, string senha)
        {
            using (var conection = Conexao.ConexaoDB.Conectar())
            {
                var sql = Scripts.Usuario.ValidaLogin;

                SqlCommand cmd = new SqlCommand(sql, conection);
                cmd.Parameters.Add("Login", SqlDbType.VarChar).Value = login;
                cmd.Parameters.Add("Senha", SqlDbType.VarChar).Value = senha;
                var rd = Convert.ToInt16(cmd.ExecuteScalar());
                return rd == 1;
            }
        }
    }
}
